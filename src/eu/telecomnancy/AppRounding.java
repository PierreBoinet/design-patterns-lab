package eu.telecomnancy;

import eu.telecomnancy.sensor.RoundingSensorDecorator;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppRounding {

    public static void main(String[] args) {
        ISensor sensor = new RoundingSensorDecorator(new TemperatureSensor());
        new ConsoleUI(sensor);
    }

}
