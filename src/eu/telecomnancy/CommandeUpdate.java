package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeUpdate  extends AbstractCommande{

	public CommandeUpdate(ISensor s) {
		super(s);
	}

	@Override
	public void execute() {
		try {
			sensor.update();
		} catch (SensorNotActivatedException e) {
		}
	}
}