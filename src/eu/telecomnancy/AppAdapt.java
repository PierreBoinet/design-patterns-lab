package eu.telecomnancy;

import eu.telecomnancy.sensor.Adapter;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppAdapt {
    public static void main(String[] args) {
        ISensor sensor = new Adapter();
        new ConsoleUI(sensor);
    }
}
