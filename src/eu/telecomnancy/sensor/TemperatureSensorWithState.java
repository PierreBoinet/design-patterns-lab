package eu.telecomnancy.sensor;

import java.util.Observable;

public class TemperatureSensorWithState extends Observable implements ISensor{

	    IState state;
	    double value = 0;
	    
	    public TemperatureSensorWithState(){
	    	state = new StateOff();
	    }
	    
	    @Override
	    public void on() {
	        state = new StateOn();
	    }

	    @Override
	    public void off() {
	        state = new StateOff();
	    }
	    
	    @Override
	    public boolean getStatus() {
	    	if(state instanceof StateOn){
	        	return true;
	        }else{
	        	return false;
	        }
	    }

	    @Override
	    public void update() throws SensorNotActivatedException {
	        value = state.update();
	        setChanged();
        	notifyObservers();
	    }

	    @Override
	    public double getValue() throws SensorNotActivatedException {
	        if (state instanceof StateOn)
	            return value;
	        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	    }

	}
