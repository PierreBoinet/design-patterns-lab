package eu.telecomnancy.sensor;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class RoundingSensorDecorator extends AbstractSensorDecorator implements ISensor {

	public RoundingSensorDecorator(ISensor s) {
		super(s);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		DecimalFormat df = new DecimalFormat("#");
		df.setRoundingMode(RoundingMode.HALF_UP);
		return new Double(df.format(sensor.getValue()));
	}
	
}
