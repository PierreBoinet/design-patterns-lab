package eu.telecomnancy.sensor;

import java.util.Observable;

public class Adapter extends Observable implements ISensor {
	
	private LegacyTemperatureSensor sensor;
	private double value;
	
	public Adapter(){
		sensor = new LegacyTemperatureSensor();
		value=sensor.getTemperature();
	}

	@Override
	public void on() {
		if(!sensor.getStatus())sensor.onOff();
	}

	@Override
	public void off() {
		if(sensor.getStatus())sensor.onOff();
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		value=sensor.getTemperature();
		setChanged();
    	notifyObservers();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return value;
	}
	
}
