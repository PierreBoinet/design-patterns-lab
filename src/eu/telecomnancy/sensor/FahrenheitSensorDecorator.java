package eu.telecomnancy.sensor;

public class FahrenheitSensorDecorator extends AbstractSensorDecorator implements ISensor {

	public FahrenheitSensorDecorator(ISensor s) {
		super(s);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue()*1.8+32;
	}

}
