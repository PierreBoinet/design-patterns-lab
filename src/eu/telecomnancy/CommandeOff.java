package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOff extends AbstractCommande{

	public CommandeOff(ISensor s) {
		super(s);
	}

	@Override
	public void execute() {
		sensor.off();
	}
}