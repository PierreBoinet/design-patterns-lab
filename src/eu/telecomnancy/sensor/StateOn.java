package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOn implements IState {

	@Override
	public double update() throws SensorNotActivatedException{
		return new Random().nextDouble() * 100;
	}

}
