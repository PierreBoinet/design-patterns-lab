package eu.telecomnancy.sensor;

public class StateOff implements IState {

	public double update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

}
