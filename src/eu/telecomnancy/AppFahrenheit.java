package eu.telecomnancy;

import eu.telecomnancy.sensor.FahrenheitSensorDecorator;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppFahrenheit {



    public static void main(String[] args) {
        ISensor sensor = new FahrenheitSensorDecorator(new TemperatureSensor());
        new ConsoleUI(sensor);
    }

}
