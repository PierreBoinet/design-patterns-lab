package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensorWithState;
import eu.telecomnancy.ui.ConsoleUI;

public class AppState {


    public static void main(String[] args) {
        ISensor sensor = new TemperatureSensorWithState();
        new ConsoleUI(sensor);
    }

}
