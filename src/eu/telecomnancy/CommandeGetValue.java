package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeGetValue  extends AbstractCommande{

	public CommandeGetValue(ISensor s) {
		super(s);
	}

	@Override
	public void execute() {
		try {
			sensor.getValue();
		} catch (SensorNotActivatedException e) {
		}
	}
}