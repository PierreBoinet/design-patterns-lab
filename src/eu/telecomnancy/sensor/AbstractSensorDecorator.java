package eu.telecomnancy.sensor;

abstract class AbstractSensorDecorator implements ISensor {

	ISensor sensor;
	
	public AbstractSensorDecorator(ISensor s){
		sensor = s;
	}
	
	@Override
	public void on() {
		sensor.on();

	}

	@Override
	public void off() {
		sensor.off();

	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();

	}
}
