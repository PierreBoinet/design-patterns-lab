package eu.telecomnancy.sensor;

public interface IState {
	double update() throws SensorNotActivatedException;
}
