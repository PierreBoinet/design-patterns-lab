package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOn extends AbstractCommande{

	public CommandeOn(ISensor s) {
		super(s);
	}

	@Override
	public void execute() {
		sensor.on();
	}
}
